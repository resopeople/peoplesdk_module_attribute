<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/specification/library/ConstSpecification.php');
include($strRootPath . '/src/specification/helper/AttrSpecHelper.php');

include($strRootPath . '/src/type/library/ConstType.php');
include($strRootPath . '/src/type/boot/TypeBootstrap.php');