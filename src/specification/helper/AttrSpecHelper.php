<?php
/**
 * Description :
 * This class allows to define attribute specification helper class.
 * Attribute specification helper allows to provide features,
 * for attribute specification.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_attribute\specification\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use people_sdk\attribute\specification\library\ToolBoxAttrSpec;
use people_sdk\attribute\specification\model\repository\AttrSpecRepository;



class AttrSpecHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Attribute specification repository instance.
     * @var AttrSpecRepository
     */
    protected $objAttrSpecRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrSpecRepository $objAttrSpecRepository
     */
    public function __construct(
        AttrSpecRepository $objAttrSpecRepository
    )
    {
        // Init properties
        $this->objAttrSpecRepository = $objAttrSpecRepository;

        // Call parent constructor
        parent::__construct();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of string attribute data types,
     * from specified attribute specification,
     * available for attribute entities.
     *
     * Attribute specification repository execution configuration array format:
     * @see ToolBoxAttrSpec::getTabDataType() configuration array format.
     *
     * @param AttrSpecInterface $objAttrSpec
     * @param null|array $tabAttrSpecRepoExecConfig = null
     * @return null|array
     */
    public function getTabDataType(
        AttrSpecInterface $objAttrSpec,
        array $tabAttrSpecRepoExecConfig = null
    )
    {
        // Return result
        return ToolBoxAttrSpec::getTabDataType(
            $objAttrSpec,
            $this->objAttrSpecRepository,
            $tabAttrSpecRepoExecConfig
        );
    }



}