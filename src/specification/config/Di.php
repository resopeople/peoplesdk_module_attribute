<?php

use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\attribute\specification\model\repository\AttrSpecRepository;
use people_sdk\module_attribute\specification\helper\AttrSpecHelper;



return array(
    // Attribute specification services
    // ******************************************************************************

    'people_attribute_specification_repository' => [
        'source' => AttrSpecRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Attribute specification helper services
    // ******************************************************************************

    'people_attribute_specification_helper' => [
        'source' => AttrSpecHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_attribute_specification_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);