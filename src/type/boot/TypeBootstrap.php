<?php
/**
 * Description :
 * This class allows to define type module bootstrap class.
 * Type module bootstrap allows to boot type module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_attribute\type\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use people_sdk\attribute\specification\type\type_string\model\StringDataType;
use people_sdk\attribute\specification\type\type_multi_string\model\MultiStringDataType;
use people_sdk\attribute\specification\type\type_text\model\TextDataType;
use people_sdk\attribute\specification\type\type_numeric\model\NumericDataType;
use people_sdk\attribute\specification\type\type_positive_numeric\model\PositiveNumericDataType;
use people_sdk\attribute\specification\type\type_integer\model\IntegerDataType;
use people_sdk\attribute\specification\type\type_positive_integer\model\PositiveIntegerDataType;
use people_sdk\attribute\specification\type\type_boolean\model\BooleanDataType;
use people_sdk\attribute\specification\type\type_date\model\DateDataType;
use people_sdk\module_attribute\type\library\ConstType;



class TypeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Provider instance.
     * @var ProviderInterface
     */
    protected $objProvider;



    /**
     * DI: Data type collection instance.
     * @var DataTypeCollectionInterface
     */
    protected $objDataTypeCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider
     * @param DataTypeCollectionInterface $objDataTypeCollection
     */
    public function __construct(
        AppInterface $objApp,
        ProviderInterface $objProvider,
        DataTypeCollectionInterface $objDataTypeCollection
    )
    {
        // Init properties
        $this->objProvider = $objProvider;
        $this->objDataTypeCollection = $objDataTypeCollection;

        // Call parent constructor
        parent::__construct($objApp, ConstType::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Get data types
        $tabDataType = array_map(
            function($strClassPath)
            {
                return $this->objProvider->getFromClassPath($strClassPath);
            },
            array(
                StringDataType::class,
                MultiStringDataType::class,
                TextDataType::class,
                NumericDataType::class,
                PositiveNumericDataType::class,
                IntegerDataType::class,
                PositiveIntegerDataType::class,
                BooleanDataType::class,
                DateDataType::class
            )
        );

        // Hydrate data type collection
        $this->objDataTypeCollection->setTabDataType($tabDataType);
    }



}