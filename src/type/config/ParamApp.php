<?php

use people_sdk\attribute\specification\type\type_string\model\StringDataType;
use people_sdk\attribute\specification\type\type_multi_string\model\MultiStringDataType;
use people_sdk\attribute\specification\type\type_text\model\TextDataType;
use people_sdk\attribute\specification\type\type_numeric\model\NumericDataType;
use people_sdk\attribute\specification\type\type_positive_numeric\model\PositiveNumericDataType;
use people_sdk\attribute\specification\type\type_integer\model\IntegerDataType;
use people_sdk\attribute\specification\type\type_positive_integer\model\PositiveIntegerDataType;
use people_sdk\attribute\specification\type\type_boolean\model\BooleanDataType;
use people_sdk\attribute\specification\type\type_date\model\DateDataType;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'attribute' => [
            'specification' => [
                'data_type' => [
                    'string' => [
                        /**
                         * String data type configuration array format:
                         * @see StringDataType configuration format.
                         */
                        'config' => []
                    ],

                    'multi_string' => [
                        /**
                         * Multi string data type configuration array format:
                         * @see MultiStringDataType configuration format.
                         */
                        'config' => []
                    ],

                    'text' => [
                        /**
                         * Multi string data type configuration array format:
                         * @see TextDataType configuration format.
                         */
                        'config' => []
                    ],

                    'numeric' => [
                        /**
                         * Numeric data type configuration array format:
                         * @see NumericDataType configuration format.
                         */
                        'config' => []
                    ],

                    'positive_numeric' => [
                        /**
                         * Positive numeric data type configuration array format:
                         * @see PositiveNumericDataType configuration format.
                         */
                        'config' => []
                    ],

                    'integer' => [
                        /**
                         * Integer data type configuration array format:
                         * @see IntegerDataType configuration format.
                         */
                        'config' => []
                    ],

                    'positive_integer' => [
                        /**
                         * Positive integer data type configuration array format:
                         * @see PositiveIntegerDataType configuration format.
                         */
                        'config' => []
                    ],

                    'boolean' => [
                        /**
                         * Boolean data type configuration array format:
                         * @see BooleanDataType configuration format.
                         */
                        'config' => []
                    ],

                    'date' => [
                        /**
                         * Date data type configuration array format:
                         * @see DateDataType configuration format.
                         */
                        'config' => []
                    ]
                ]
            ]
        ]
    ]
);