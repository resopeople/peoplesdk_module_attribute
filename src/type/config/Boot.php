<?php

use people_sdk\module_attribute\type\boot\TypeBootstrap;



return array(
    'people_attribute_type_bootstrap' => [
        'call' => [
            'class_path_pattern' => TypeBootstrap::class . ':boot'
        ]
    ]
);